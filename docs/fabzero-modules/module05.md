# 5. Dynamique de groupe et projet final

## Problem/Objective Tree Analysis

Un arbre de problèmes et un arbre d'objectifs sont deux outils utilisés dans la planification et la gestion de projets pour identifier les causes sous-jacentes d'un problème et pour développer un ensemble d'objectifs et de stratégies pour résoudre ce problème.


### Porblem Tree

Un arbre de problèmes est une représentation visuelle des différents facteurs qui contribuent à un problème. Il est composé d'un problème racine, qui est le problème à résoudre, et de branches qui représentent les différentes causes du problème. Les branches peuvent être décomposées en sous-branches, qui représentent les facteurs sous-jacents contribuant à chaque cause.

![](images/problemtree.png) 


### Objective Tree

Un arbre d'objectifs est un outil utilisé pour développer un ensemble d'objectifs et de stratégies pour atteindre un objectif spécifique. Il est composé d'un objectif global, qui représente le résultat souhaité ou l'objectif, et de branches qui représentent les différents objectifs qui doivent être atteints pour atteindre l'objectif global. Chaque objectif est ensuite décomposé en sous-objectifs ou en stratégies, qui représentent les actions spécifiques à prendre pour atteindre chaque objectif.

![](images/objectivetree.png) 


Ces deux outils sont utiles pour la planification et la gestion de projets car ils fournissent une représentation visuelle claire du problème ou de l'objectif et aident les équipes de projet à identifier les causes et les stratégies nécessaires pour atteindre leurs objectifs.

## Session 1 : Formation du groupe pour le projet final

Lors de la première session, nous avons été invités à sélectionner des objets qui représentaient une problématique qui nous intéressait. Nous avons placé nos objets sur le sol et les personnes ayant des objets ou des thèmes similaires se sont regroupées pour former les groupes pour le projet final. Notre groupe était composé de Moi (Aymane) avec un ticket de transport en commun, de Lorea avec une pièce de 1€, de Mariam avec une petite voiture , Matthew avec une étiquette d'un pot de yaourt au soja et finalement Virginie avec une clé de voiture.
![](images/444.png) 


Nous avons pensé que nos objets étaient similaires et liés à un ou plusieurs problèmes de transport. Nous avons écrit l'idée derrière notre objet sur des post-it et avons placé nos noms sur un axe allant de "Je ne suis pas d'accord pour travailler sur ce thème" à "Je suis très enthousiaste à l'idée de travailler sur ce thème" pour déterminer quel thème convenait à tous.
![](images/222.png)

![](images/333.png)

 
Nous avons choisi un thème lié à la pollution liée aux transports et identifié des problèmes sous-jacents, notamment le transport commercial, les combustibles fossiles, la pollution de l'air, la pénurie de matières premières et le manque d'éducation et d'information sur ces sujets.
![](images/111.png)



## Session 2 : Formation en gestion de projet

Lors de la deuxième session, la gestion de projet a été introduite en étudiant différents outils. 
L'un des outils intéressants était la technique des doigts, qui permettait aux individus de prendre tour à tour la parole de manière équitable dans un groupe. La division des tâches et des rôles était une autre idée qui nous a incités à nous concentrer sur les compétences de chaque personne et à diviser les grandes tâches en sous-tâches opérationnelles. Nous avons assigné des rôles au groupe, y compris animateur, secrétaire, communication et gestion du temps, en utilisant la méthode des doigts motivés. La structuration des réunions en gestion de projet a également été discutée et la structure suivante a été présentée : météo d'entrée, rôles de la journée, fixation de l'ordre du jour, fixation de l'ordre du jour et répartition des rôles/tâches futures, météo de sortie et clôture de la réunion.  

![](images/122.png)






