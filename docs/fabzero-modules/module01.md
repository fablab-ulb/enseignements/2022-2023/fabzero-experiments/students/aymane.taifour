# 1. Gestion de projet et documentation

Git is a powerful tool that can help you write better documentation and share it with others , which is important for project management. With Git, you can easily track changes to your documentation over time, collaborate with others on the same documentation, and roll back to previous versions if needed. Git also makes it easy to work on documentation from different computers or locations, as all changes are synchronized to a central repository.

## Basic Git Commands

Here are some of the basic Git commands you'll need to know:

### git init

This command initializes a new Git repository. To use it, navigate to the directory where you want to create the repository and enter git init.

### git clone

This command creates a copy of a remote repository on your local machine. To use it, enter git clone [url], where url is the URL of the remote repository.

### git add

This command stages changes to your files for inclusion in the next commit. To use it, enter git add [file], where file is the name of the file you want to stage.
You can also use git add . to add all the files missing.

### git commit

This command creates a new commit with the changes you've staged. To use it, enter git commit -m "Commit message", where "Commit message" is a brief description of the changes you've made.

### git push

This command uploads your local changes to the remote repository. To use it, enter git push.

### git pull

This command downloads changes from the remote repository to your local machine. To use it, enter git pull.

### git status

This command displays the status of your Git repository, including which files have been modified and which files are staged for commit. To use it, enter git status.


Using Git to manage your documentation can make the process of writing and sharing documentation much easier and more efficient. By mastering the basic Git commands, you'll be able to track changes to your documentation over time, collaborate with others, and make sure that everyone is always working with the most up-to-date version of the documentation.

## Compressing Images and Keeping Storage Space Low

### Use Image Compression Software

Image compression software can help you reduce the file size of your images without sacrificing quality. There are many free and paid tools available for this purpose, including:

- [ImageOptim](https://imageoptim.com/fr)  (Mac)
- [JPEGmini ](https://www.jpegmini.com/) (Mac and Windows)

### Resize Images

Resizing images to a smaller resolution can significantly reduce their file size. If your images are larger than they need to be, consider resizing them to a more appropriate size before uploading them to your website or application. You can use image editing software like Adobe Photoshop or free alternatives.

### Convert Images to Web-Friendly Formats

Converting images to web-friendly formats can also help to reduce file size. The most common image formats for the web are JPEG, PNG, and GIF. JPEG is best for photographs and complex images with many colors, while PNG is best for simple graphics and images with transparent backgrounds. GIF is best for simple animations.

When saving images in these formats, be sure to choose the appropriate compression level to achieve the smallest file size possible without sacrificing quality.

## Project management principles 


As-you-work documentation is a great example of project management principles in action. By documenting your progress as you work, you can ensure that you stay on track and meet your objectives. Additionally, this documentation can help you identify areas where you can improve and make necessary adjustments. In my work, I plan on using project management principles to ensure that I'm meeting my objectives and completing my tasks efficiently. By defining the scope of my work, creating a plan, assigning roles and responsibilities, monitoring progress, and evaluating results, I can ensure that I'm on track to meet my goals and deliver quality work to my team and clients 

### Markdown for documentaion

Markdown is a simple and easy-to-use markup language that allows you to format text using easy-to-learn commands. 
With Markdown, I could quickly format my text using basic commands like **bold** , *italic* and ``code`` I could also add headings, links, and images using simple syntax. This made my documentation more readable and professional-looking without spending a lot of time formatting it. Overall, Markdown helped me save time and effort while creating documentation, which allowed me to focus on more important aspects of my projects.
Here are some basics syntax :
 Use "#" symbols to create headers, "**" for bold text and "*" for italic text. To make a bulleted list, use "-" and for a numbered list, use "1.". To add links and images, use ``[Link Text](URL)``  and ``[Text](Image URL)`` respectively. To format text as code, use backticks. ( `` before and at the end of your code)
These are just the basics of Markdown, but they will help you create beautiful, well-formatted documents quickly and easily.













