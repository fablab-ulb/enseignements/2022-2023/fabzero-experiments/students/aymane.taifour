/*

    Description : Fixed Fixed Beam (PHYS-F513 Assignement 2)
    
    Author : Aymane Taifour
    
    Date : 01/03/2023
    
    Lincence : Creative Commons : Attribution-ShareAlike 4.0 International [CC BY-SA 4.0] 
    https://creativecommons.org/licenses/by-sa/4.0/)
     

*/




$fn=250;
u=2.5;
h=4;  // hauteur
w=1;  // largeur
l=30; // longueur 


pos_base_droite=l/2+u;
pos_base_gauche=-l/2-u;


ext_diameter=5;
int_diameter=3;

// 

difference()
{
    union()
    {
        hull()
        {     //base droite
            translate([0,pos_base_droite-2*u,0])          cylinder (h = h, d=ext_diameter, center=true);
            translate([0,pos_base_droite,0])              cylinder (h = h, d=ext_diameter, center=true);
        }
  
        hull()
        {     //base gauche
            translate([0,pos_base_gauche,0])               cylinder (h = h, d=ext_diameter, center=true);
            translate([0,pos_base_gauche+2*u,0])           cylinder (h = h, d=ext_diameter, center=true);
        } 
    
 
        translate([0,0,0])     //barre      
        cube([w, l, h], center=true);
    
        
            
        
    }
    union()// trous
    {   //base droite
        translate([0,pos_base_droite-2*u,0])  cylinder (h = h+1, d=int_diameter, center=true);
        translate([0,pos_base_droite,0])      cylinder (h = h+1, d=int_diameter, center=true);
        //base gauche
        translate([0,pos_base_gauche,0])      cylinder (h = h+1, d=int_diameter, center=true);
        translate([0,pos_base_gauche+2*u,0])  cylinder (h = h+1, d=int_diameter, center=true);       
        
     }
 }
 


