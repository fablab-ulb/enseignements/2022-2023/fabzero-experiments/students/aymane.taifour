# 4. Laser Cutter : Epilog

Dans cette activité, nous avons utilisé la machine de Laser Cutter Epilogue pour créer un objet en 3D à partir d'un dessin en 2D.

## Demonstration et description du processus de conception 2D

Le processus de conception 2D implique la création d'un dessin numérique qui peut être utilisé pour la découpe laser. Nous avons utilisé un logiciel de dessin vectoriel pour créer notre dessin en 2D, puis nous l'avons enregistré au format SVG. Il est important de noter que chaque élément du dessin doit être défini avec un trait de moins de 0,25 mm et que les éléments à découper doivent être identifiés par la couleur de leur trait.

## Paramètres de l'Epilog

L'Epilog offre deux modes de découpe :

**Gravure** : qui opère comme une imprimante basique en procédant par couches successives.
**Vectoriel** : le mode préféré, qui suit le tracé défini par le fichier SVG.
Nous privilégions le mode vectoriel, mais nous ajustons également les paramètres selon nos besoins.
La fréquence est généralement laissée à 30.
La puissance, qui détermine l'intensité du laser, peut être augmentée pour obtenir une découpe plus profonde, mais au détriment de la précision.
La vitesse quant à elle, détermine la vitesse de déplacement du laser.
Si la découpe n'est pas satisfaisante dès le premier essai, il est possible de redémarrer la machine sans déplacer le matériau.
Il est recommandé de réaliser des tests préalables sur le matériau à découper pour déterminer les paramètres optimaux.

## Test de materiel

J'ai effectué un Material Test sur l'Epilog pour déterminer les meilleurs paramètres pour couper et graver différents types de matériaux.

Pour cela, j'ai utilisé un fichier SVG de test que j'ai trouvé en ligne. Le fichier contenait plusieurs formes simples à différentes tailles et épaisseurs de ligne.
J'ai d'abord sélectionné un petit morceau de matériau que je voulais tester et je l'ai fixé sur le lit de la machine. J'ai ensuite ouvert le fichier SVG dans le logiciel d'Epilog et sélectionné le mode de coupe vectorielle.
![](images/materialtest.png) 



J'ai commencé par régler la fréquence et j'ai ajusté la puissance et la vitesse pour trouver les meilleurs paramètres pour mon matériau. J'ai effectué plusieurs coupes et gravures avec des réglages différents jusqu'à ce que je sois satisfait du résultat.

Une fois que j'ai trouvé les paramètres optimaux, j'ai noté ces informations pour pouvoir les utiliser à l'avenir lorsque je travaillerai avec ce type de matériau.

Le Material Test est une étape importante pour garantir que les coupes et gravures sont précises et efficaces sur différents types de matériaux. Je recommande fortement de faire un Material Test avant de commencer tout projet sur l'Epilog.
![](images/material.png) 











