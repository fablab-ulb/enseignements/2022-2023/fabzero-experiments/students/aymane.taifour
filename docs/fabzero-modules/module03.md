# 3. Impression 3D

3D printing, also known as additive manufacturing, is a process of creating three-dimensional objects by depositing layers of material on top of one another until the final object is formed. This technology has become increasingly popular in recent years due to its ability to produce complex geometries with high accuracy and precision.

## From SCAD to 3D manufacturing format (3mf)

To prepare our digital design for 3D printing, it must be converted into a format that can be read by the printer. In our case, we converted our design file in the OpenSCAD software into the 3D manufacturing format (3mf) using PrusaSlicer software. 

This conversion is necessary because the printer needs to know the exact dimensions, shape, and position of the object to print it correctly.



## PrusaSlicer
 
We used PrusaSlicer software to set up and prepare our design for 3D printing. PrusaSlicer is a free, open-source software that allows you to adjust various settings to achieve the desired print quality. We chose PrusaSlicer because it is user-friendly, easy to navigate, and has excellent support for our 3D printer model.




### Object Setup in PrusaSlicer

We set up our fixed-fixed beam Lego compatible design in PrusaSlicer to ensure that it was in an optimal position for printing. This involved adjusting the orientation and support structures to minimize the risk of errors and ensure a successful print.
We also adjusted the **Impression settings** , **filament settings** and **the printing machine settings**.

![](images/fixedfixed-prusa.png) 


## Printing 

### Tool used 

To determine the optimal parameters for our design, we printed a ruler with holes of different diameters and tested them to see which parameters would ensure that LEGOs fit in correctly. After testing, we determined that the space between the holes should be 3.2mm, and the diameter of the holes should be 5.1mm. We then used these parameters to adjust our design in PrusaSlicer, ensuring that our fixed-fixed beam Lego compatible design would fit LEGOs correctly.

![](images/regle.jpg) 


### G-code 

With the G-code file created, we were ready to move on to the printing phase. We loaded the file onto an SD card , and then into the 3D printer and began the printing process, which involved melting and extruding plastic filament to build up our design layer by layer. By slicing the design file in PrusaSlicer and generating a G-code file, we were able to optimize the print settings and create a precise and functional 3D printed object.

### Printing Machines 

We printed our fixed-fixed beam Lego compatible design using the FABLAB 3D printing machines available at ULB FABLAB. 

Specifically, we used the Original Prusa i3 **MK3S and MK3S+ 3D printers**, which are known for their high-quality prints and reliability. 
The 3D printing process was carefully monitored to ensure a successful print, and the end result was a functional and precise design.


### Object Rendering

This is the final result of the **Fixed-Fixed Beam (LEGO compatible)** from FlexLink : 

![](images/fixedfixed.jpg) 

















