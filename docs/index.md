## Welcome

Hello!

Welcome to my FabLab website for the [2022-2023 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/).




## About me

Hi! I am Aymane Taifour. I am a computer science student at ULB based in Brussels.
This website will contain informations about me and my work through the class.

## My background

I grew up in Marrakech and lived there before moving to Brussels in order to continue my studies.

## Previous work

Although my previous work experience has primarily been focused on university projects, I have gained valuable computer science skills in programming languages such as Python and C++. Through these projects, I have developed a strong understanding of CS concepts and have sharpened my problem-solving abilities.