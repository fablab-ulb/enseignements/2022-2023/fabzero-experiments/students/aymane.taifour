# 2. Computer Aided Design

Computer-Aided Design (CAD) refers to the use of computer software to create, modify, and optimize designs for a wide range of products and structures. CAD is widely used in various industries, including architecture, engineering, manufacturing, product design, and many more.

CAD software allows designers to create detailed 2D or 3D models of their designs, which can be viewed and modified from any angle, and can also be used to simulate real-world conditions and test the functionality of the design. This enables designers to refine and improve their designs more quickly and accurately than with traditional methods, reducing the time and cost involved in the design process.

CAD software also allows designers to share their designs easily with others, collaborate on designs with team members in real-time, and to produce detailed and accurate documentation for manufacturing, construction, and other purposes. 


## CAD Softwares 

- [Inkscape](./Inkscape.md)
- [OpenSCAD](./OpenSCAD.md)
- [FreeCAD](./FreeCAD.md)
- **Many** others... :
  - [Antimony](https://github.com/mkeeter/antimony) 
  - [Fusion360](https://www.autodesk.com/products/fusion-360/)
  - Rhino/[Grasshopper](https://www.grasshopper3d.com/)
  - [Onshape](https://www.onshape.com/products/education)

## Work ( FlexLink )

The script is written in the OpenSCAD programming language. The code creates a 3D model of a Fixed Fixed Beam from FlexLink , LEGO compatible. 

The code defines the dimensions and shapes of the various components of the beam, including the base, the bar, and the holes. It uses basic geometric shapes like cylinders and cubes to create the design. 

The script also specifies the position of each component and the diameter and height of the cylinders used.

The license specified at the beginning of the script indicates that the code is released under the Creative Commons Attribution-ShareAlike 4.0 International license.

### Parameters 


**$fn**: the number of facets used to render curved shapes in the design.

**u**: the distance between the bar and the base.

**h**: the height of the beam.

**w**: the width of the beam.

**l**: the length of the beam.

**ext_diameter**: the diameter of the cylinders used to create the outer surface of the base.

**int_diameter**: the diameter of the cylinders used to create the holes in the base.

These parameters can be adjusted to modify the design of the Fixed Fixed Beam, allowing for customization of the beam's dimensions and shape to suit specific engineering requirements.


### Code example 

**For example** : 

The "difference()" function subtracts four cylindrical holes from the main body of the beam. The "union()" function groups the three components of the beam together.


```difference()
{
    union()
    {
        hull()
        {     //base droite
            translate([0,pos_base_droite-2*u,0])          cylinder (h = h, d=ext_diameter, center=true);
            translate([0,pos_base_droite,0])              cylinder (h = h, d=ext_diameter, center=true);
        }
  
        hull()
        {     //base gauche
            translate([0,pos_base_gauche,0])               cylinder (h = h, d=ext_diameter, center=true);
            translate([0,pos_base_gauche+2*u,0])           cylinder (h = h, d=ext_diameter, center=true);
        } 
    
 
        translate([0,0,0])     //barre      
        cube([w, l, h], center=true);
    
        
            
        
    }
  ```





### 3D Model

![Rendering of Fixed Fixed Beam in OpenSCAD](images/scad_model.png)



